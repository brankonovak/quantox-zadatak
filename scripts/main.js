var $body = $('body');

var rad = function (x) {
	return x * Math.PI / 180;
};

var getDistance = function (p1, p2) {
	var R = 6378137; // Earth’s mean radius in meter
	var dLat = rad(p2.lat - p1.lat);
	var dLong = rad(p2.lng - p1.lng);
	var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
	Math.cos(rad(p1.lat)) * Math.cos(rad(p2.lat)) *
	Math.sin(dLong / 2) * Math.sin(dLong / 2);
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	var d = R * c;
	return d; // returns the distance in meter
};

const API_KEY = 'AIzaSyC55t6DdgRualAPF7ypkhqgCuOgUR34axw';
let map,
	placesService,
	userCenter,
	markers = [],
	atms = [],
	curentAtms = [];

let belgrade = {
	lat: 44.787197,
	lng: 20.457273
}

function initMap () {
	navigator.geolocation.getCurrentPosition(function (pos) {
		userCenter = {
			lat: pos.coords.latitude,
			lng: pos.coords.longitude
		}

		createMap(userCenter);
		getAtms(userCenter, (atms) => {
			getAtmsDetails(userCenter, atms);
		});
	}, function error (err) {
		createMap();
		getAtms(belgrade, (atms) => {
			getAtmsDetails(belgrade, atms);
		});
	});

	createSearch();
}

function getAtms (centerPoint, callback) {
	placesService = new google.maps.places.PlacesService(map);
	placesService.nearbySearch({
		location: centerPoint,
		radius: 10000,
		type: ['atm']
	}, function (res) {
		typeof callback === 'function' ? callback(res) : null;
	});
}

function getAtmsDetails (centerPoint, atmsArray) {
	let counter = 0;

	for (let i in atmsArray) {
		placesService.getDetails({ placeId: atmsArray[i].place_id }, function (place, status) {
			counter++;

			if (status == google.maps.places.PlacesServiceStatus.OK) {
				let placePos = {
					lat: atmsArray[i].geometry.location.lat(),
					lng: atmsArray[i].geometry.location.lng()
				}

				let dist = getDistance(placePos, centerPoint);

				place.distance = parseInt(dist);
				place.coords = {
					lat: place.geometry.location.lat(),
					lng: place.geometry.location.lng()
				}
				place.coords = JSON.stringify(place.coords);
				place.staticMap = `https://maps.googleapis.com/maps/api/staticmap?center=${placePos.lat},${placePos.lng}&zoom=14&size=100x60&key=${API_KEY}`

				atms.push(place);
			}

			counter === atmsArray.length ? showAtms(atms) : null;
		});
	}
}

function showAtms (atms, sort='asc') {
	let source = $("#template").html(),
		template = Handlebars.compile(source),
		$atmsList = $('.js-atms-list');

	curentAtms = atms;

	// Sort
	atms.sort((a, b) => {
		return sort === 'asc' ? a.distance > b.distance : a.distance < b.distance;
	});

	let html = template(atms.slice(0, 10));
	$atmsList.html(html);
}

function createMap (center = belgrade) {
	map = new google.maps.Map(document.getElementById('map'), {
		zoom: 16,
		center: center
	});

	let marker = new google.maps.Marker({
		position: center,
		map: map
	});
}

function createSearch () {
	$('.js-search').on('keyup cut paste', function () {
		let $this = $(this),
			text = $this.val().trim().toLowerCase();

		if (text.length > 1) {
			let results = atms.filter((atm) => {
				if ((atm.name.toLowerCase().indexOf(text) !== -1 ||
					atm.formatted_address.toLowerCase().indexOf(text) !== -1)) {
					return atm;
				}
			});
			showAtms(results);
		} else {
			showAtms(atms);
		}
	});
}

$('.js-sort-btn').on('click', function () {
	let sort = $(this).data('sort');
	showAtms(curentAtms, sort);
});

$(document).on('click', '.js-atm', function () {
	// Clear markers
	for (let i in markers) {
		markers[i].setMap(null);
	}

	let coords = $(this).data('coords');

	map.setCenter(coords);

	let marker = new google.maps.Marker({
		position: coords,
		map: map,
		animation: google.maps.Animation.DROP,
		icon: 'images/green-pin.png'
	});
	markers.push(marker);
});

$(document).on('click', '.js-switch-btn, .js-atm', function () {
	if (matchMedia("(max-width: 768px)").matches) {
		$body.toggleClass('css-js-list-view-inactive');
	}
});

$(window).on('resize', function () {
	if (matchMedia("(min-width: 768px)").matches) {
		$body.removeClass('css-js-list-view-inactive');
	}
})